package Base;

public class SearchText {


    String EnterText;
    String ResultText;

    public String getEnterText() {
        return EnterText;
    }

    public void setEnterText(String enterText) {
        EnterText = enterText;
    }

    public String getResultText() {
        return ResultText;
    }

    public void setResultText(String resultText) {
        ResultText = resultText;
    }
}
