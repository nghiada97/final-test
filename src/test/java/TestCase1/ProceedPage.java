package TestCase1;

import Base.WebDrive;
import models.CartIteam;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ProceedPage {

    WebDrive drive;

    private static final By ProceedButton = By.xpath("//p/a[@title=\"Proceed to checkout\"]");
    private static final By SigninEmailField = By.xpath("//div[@class=\"form_content clearfix\"]//input[@name=\"email\"]");
    private static final By PasswordField = By.xpath("//input[@id=\"passwd\"]");
    private static final By ProceedAddress = By.xpath("//button[@name=\"processAddress\"]");
    private static final By SigninButton = By.xpath("//button[@id=\"SubmitLogin\"]");
    private static final By TermsOfService = By.xpath("//div[@class=\"checker\"]");
    private static final By ProceedShipping = By.xpath("//button[@name=\"processCarrier\"]");

    private static final By PayByCheck = By.xpath("//a[@class=\"cheque\"]");
    private static final By ComfirmOrder = By.xpath("//p[@id=\"cart_navigation\"]//button[@type=\"submit\"]");
    private static final By SuccessfullyMess = By.xpath("//p[@class=\"alert alert-success\"]");
    public static By totalPrice = By.xpath("//span[@id=\"total_price\"]");
    private static final By QtyField = By.xpath("//tr[1]//td[@class=\"cart_quantity text-center\"]/input[2]");
    private static final By RemoveAProduct = By.xpath("//tr[2]//td[@class=\"cart_delete text-center\"]//a");
    private static final By ErrorMessInShipping = By.xpath("//p[@class=\"fancybox-error\"]");
    private static final By Close = By.xpath("//a[@class=\"fancybox-item fancybox-close\"]");
    private static final By ProductName = By.xpath("//tr[1]//p[@class=\"product-name\"]/a");


    private By UnitPrice(int i){
        return By.xpath("//tr["+i+"]//td[@class=\"cart_unit\"]/span[@class=\"price\"]/span");
    }

    private By Qty ( int i ){
        return By.xpath("//tr["+i+"]//td[@class=\"cart_quantity text-center\"]/input[1]");
    }

    public ProceedPage(WebDrive drive){
        this.drive = drive;
    }

    // Summary
    public void ClickProceedButton (){
        drive.click(ProceedButton);
    }

    public CartIteam iteam (int i){
        CartIteam cartIteam = new CartIteam();
        cartIteam.setName(drive.getText(ProductName));
        cartIteam.setPrice(drive.parsdouble(drive.getText(UnitPrice(i))));
        cartIteam.setQty(drive.getAttribute(Qty(i)));
        return cartIteam;
    }



    //Sign In
    public void SignIn(){
        drive.sendkey(SigninEmailField,"moneylove9971@gmail.com");
        drive.sendkey(PasswordField,"12345");
        drive.click(SigninButton);
    }

    public void ClickProceedAddress (){
        drive.click(ProceedAddress);
    }

    //Shipping
    public void Shipping(){

        drive.click(TermsOfService);
        drive.click(ProceedShipping);
    }

    public void DontClickOnShipping(){
        drive.click(ProceedShipping);
        Assert.assertEquals(drive.getText(ErrorMessInShipping),"You must agree to the terms of service before continuing.");
        drive.click(Close);
//        drive.getDriverChrome().manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void CheckOut(){
        drive.click(PayByCheck);
        drive.click(ComfirmOrder);
        Assert.assertEquals(drive.getText(SuccessfullyMess),"Your order on My Store is complete.");
    }

    public void ChangeQty(){
        drive.ClearText(QtyField);
        drive.sendkey(QtyField,"3");
    }

    public void Remove(){
        drive.click(RemoveAProduct);
    }
}
