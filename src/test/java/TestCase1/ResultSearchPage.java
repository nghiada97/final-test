package TestCase1;

import Base.SearchText;
import Base.WebDrive;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.Locale;

public class ResultSearchPage {

    WebDrive drive;
    SearchText text;

    private static final By proDuctName = By.xpath("//h1[@itemprop=\"name\"]");
    private By ProductName (int i) {
        return By.xpath("//ul[@class=\"product_list grid row\"]/li["+i+"]//a[@class=\"product-name\"]");
    }
    private static final By LengthProductName = By.xpath("//ul[@class=\"product_list grid row\"]/li//a[@class=\"product-name\"]");
    private static final By ProductNumber = By.xpath("//span[@class=\"heading-counter\"]");

    private static final By PriceNumber (int i) {

      return  By.xpath("//ul[@class=\"product_list grid row\"]/li[" + i + "]//div[@class=\"right-block\"]//span[@itemprop=\"price\"]");
    }

    private static final By SearchMess = By.xpath("//p[@class=\"alert alert-warning\"]");

    public ResultSearchPage(WebDrive drive){
        this.drive = drive;
    }

    public void compareTextSearch() {

        try {

            for (int i = 1; i < drive.ElementSize(LengthProductName); i++) {
                text = new SearchText();
                text.setResultText(drive.getText(ProductName(i)));
                Assert.assertTrue(text.getResultText().toLowerCase(Locale.ROOT).contains("dress"));
            }
        } catch (Exception e){
            throw e;
        }
    }

    public void CompareProductNumber(){
        String number = drive.getText(ProductNumber).trim().substring(0,1);
        int intNumber = Integer.parseInt(number);

        Assert.assertEquals(drive.ElementSize(LengthProductName),intNumber);
    }

    public void ProductPrice(){
        for (int i = 1; i < drive.ElementSize(LengthProductName); i++) {
            drive.displayed(PriceNumber(i));
        }
    }

    public void CheckSearchMess(){
        String Mess = drive.getText(SearchMess);

        Assert.assertEquals(Mess,"No results were found for your search \"dresSSss\"");
    }


}
