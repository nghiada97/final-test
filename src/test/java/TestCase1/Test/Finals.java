package TestCase1.Test;


import Base.WebDrive;
import TestCase1.*;
import models.CartIteam;
import models.Order;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Finals {

    WebDrive drive;

//   @BeforeMethod ()
//   @Parameters ("browser")
//    public void init(String browser) {
//      if (browser.equalsIgnoreCase("Chrome")) {
//         drive = new WebDrive();
//         drive.getDriverChrome();
//         drive.getDriverFireFox().get("http://automationpractice.com/");
//
//      } else if (browser.equalsIgnoreCase("firefox")){
//         drive = new WebDrive();
//         drive.getDriverFireFox();
//         drive.getDriverFireFox().get("http://automationpractice.com/");
//      }
//
//   }

   @BeforeMethod
   public void init(){
      drive = new WebDrive();
      drive.getDriverFireFox();
      drive.getDriverFireFox().get("http://automationpractice.com/");
   }

   @AfterMethod
    public void teardown(){
       drive.quit();
   }

   @Test(testName = "Create account with wrong email")
    public void CreateWrongAccount(){
       HomePage homePage = new HomePage(drive);
       homePage.ClickSignIn();

       SignInPage signInPage = new SignInPage(drive);
       signInPage.CreateAccount("abc123");
       signInPage.ShowErrorMess();
   }

   @Test (testName = "Create Successfully Account")
    public void CreateSuccessAccount(){
       HomePage homePage = new HomePage(drive);
       homePage.ClickSignIn();

       SignInPage signInPage = new SignInPage(drive);
       signInPage.CreateAccount("nghiada97@gmail.com");

       CreateAccountPage create = new CreateAccountPage(drive);
       create.CreateAccount();

       MyAccountPage account = new MyAccountPage(drive);
       account.ShowMyaccount();

   }

   @Test(testName = "NewsLetter")
   public void Newsletter(){
      HomePage homePage = new HomePage(drive);
      homePage.SubmitNewsletter();
     // homePage.SuccessfullyMess();

      NewTab newTab = new NewTab(drive);
      newTab.CheckEmail();
   }
   @Test(testName = "Contact Us")
   public void ContactUs(){
      HomePage homePage = new HomePage(drive);
      homePage.clickContactUs();

      ContactUsPage contactUs = new ContactUsPage(drive);
      contactUs.SendMessage();
      contactUs.SuccessfullyMess();
   }

   @Test(testName = "Check Search")
   public void CheckSearch(){
      HomePage homePage = new HomePage(drive);

      homePage.Sreenshot("/untitled5/Image/1.png");

      homePage.EnterSearchBar("dress");

      homePage.Sreenshot("/untitled5/Image/2.png");
      try {
         drive.CompareImage("/untitled5/Image/1.png","/untitled5/Image/2.png");
      } catch (IOException e) {
         e.printStackTrace();
      }

      homePage.ClearText();
      homePage.Sreenshot("/untitled5/Image/3.png");

      try {
         drive.CompareImage("/untitled5/Image/2.png","/untitled5/Image/3.png");
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   @Test(testName = "Search Suggestions")
   public void SearchSuggestions(){
      HomePage homePage = new HomePage(drive);
      homePage.EnterSearchBar("dress");
      homePage.CheckSuggestionsResult("dress");
      homePage.ClickSuggestionResult();

      ResultSearchPage res = new ResultSearchPage(drive);

      res.CompareProductNumber();
      res.ProductPrice();

      res.compareTextSearch();

   }

   @Test(testName = "Search with Invalid")
   public void SearchWithInvalid(){
      HomePage homePage = new HomePage(drive);
      homePage.EnterSearchBar("dresSSss");
      homePage.ClickSuggestionResult();

      ResultSearchPage searchPage = new ResultSearchPage(drive);
      searchPage.CheckSearchMess();
   }

   @Test(testName = "Buy product Successfully")
   public void BuyProduct(){
       Order order = new Order();

      HomePage homePage = new HomePage(drive);
      homePage.AddtocartAndContinue(2);
      homePage.AddtocartAndProceed();

      ProceedPage proceedPage = new ProceedPage(drive);

      for (int i = 1 ; i <= 3 ; i++){
         CartIteam iteam = proceedPage.iteam(i);
         order.addProduct(iteam);
         System.out.println(iteam.getPrice());
      }

      order.setTotalPrice(drive.parsdouble(drive.getText(ProceedPage.totalPrice)));

      double CurrentPrice = Math.round(order.getCurrentPrice()*100)/100d;

      Assert.assertEquals(CurrentPrice,order.getTotalPrice());

      proceedPage.ClickProceedButton();
      proceedPage.SignIn();
      proceedPage.ClickProceedAddress();
      proceedPage.Shipping();
      proceedPage.CheckOut();
   }

   @Test (testName = "Change info product in Cart")
   public void ChangeProductInCart(){

      Order order = new Order();

      HomePage homePage = new HomePage(drive);
      homePage.AddtocartAndContinue(5);
      homePage.ClickShoppingCart();

      ProceedPage proceedPage = new ProceedPage(drive);
      proceedPage.ChangeQty();
      proceedPage.Remove();
      try {
         Thread.sleep(2000);
      } catch (InterruptedException e) {
         e.printStackTrace();
      }

      for (int i = 1 ; i <= 4 ; i++){
         CartIteam iteam = proceedPage.iteam(i);
         order.addProduct(iteam);
         System.out.println(iteam.getPrice());
      }

      order.setTotalPrice(drive.parsdouble(drive.getText(ProceedPage.totalPrice)));

      double CurrentPrice = Math.round(order.getCurrentPrice()*100)/100d;
      Assert.assertEquals(CurrentPrice,order.getTotalPrice());

      proceedPage.ClickProceedButton();
      proceedPage.SignIn();
      proceedPage.ClickProceedAddress();
      proceedPage.DontClickOnShipping();
      proceedPage.Shipping();
      proceedPage.CheckOut();
   }

   @Test(testName = "Buy 20% discount product ")
   public void BuyDiscountProduct(){
      HomePage homePage = new HomePage(drive);
      homePage.ClickDiscountProduct();

      ProceedPage proceedPage = new ProceedPage(drive);
      proceedPage.ClickProceedButton();
      proceedPage.SignIn();
      proceedPage.ClickProceedAddress();
      proceedPage.Shipping();
      proceedPage.CheckOut();

   }


   @Test(testName = "Check view Large")
   public void CheckviewLarge(){
      HomePage homePage = new HomePage(drive);
      homePage.ClickImage();

      ProductPage productPage = new ProductPage(drive);
//      Compare size Image 1
      productPage.ImageS("/untitled5/Image/imageS.png");
      productPage.ClickImage();
      productPage.ImageL("/untitled5/Image/imageL.png");
      try {
         drive.CompareImage("/untitled5/Image/imageS.png","/untitled5/Image/imageL.png");
      } catch (IOException e) {
         e.printStackTrace();
      }
      drive.CompareSize("/untitled5/Image/imageS.png","/untitled5/Image/imageL.png");
      productPage.CompareName();

//      Compare size Image 2
      productPage.ClickViewLarge();
      productPage.ImageL1("/untitled5/Image/imageL1.png");
      try {
         drive.CompareImage("/untitled5/Image/imageS.png","/untitled5/Image/imageL1.png");
      } catch (IOException e) {
         e.printStackTrace();
      }
      drive.CompareSize("/untitled5/Image/imageS.png","/untitled5/Image/imageL1.png");
//      productPage.CompareImageSize();
      productPage.CompareName();

      //
      productPage.ChangeQtyAndAdd("0");
      productPage.NullQtyMess();
      productPage.ChangeQtyAndAdd("1");

      CartIteam Product = productPage.Iteam();

      productPage.SuccessAdd();

      ProceedPage proceedPage = new ProceedPage(drive);
      CartIteam proceed = proceedPage.iteam(1);
      Assert.assertEquals(Product.getName(),proceed.getName());

   }

   @Test(testName = "Share to Twitter")
   public void ShareToTwitter(){
      HomePage homePage = new HomePage(drive);
      homePage.ClickImage();

      ProductPage productPage = new ProductPage(drive);
      productPage.ClickTwitter();

      drive.SwitchWindown();
      TweetPage tweetPage = new TweetPage(drive);
      tweetPage.Signin();
      tweetPage.Tweet();
   }

   @Test(testName = "Write new comment")
   public void WriteNewComment(){
      HomePage homePage = new HomePage(drive);
      homePage.ClickSignIn();

      SignInPage signInPage = new SignInPage(drive);
      signInPage.Signin();

      homePage.ClickLogo();
      homePage.ClickImage();

      ProductPage productPage = new ProductPage(drive);
      productPage.WriteAComment();
   }

   @Test(testName = "Send to a friend")
   public void SendToAFriend(){
      HomePage homePage = new HomePage(drive);
      homePage.ClickImage();

      ProductPage productPage = new ProductPage(drive);
      productPage.SendAFriend();
   }
}
