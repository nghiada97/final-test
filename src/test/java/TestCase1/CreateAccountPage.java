package TestCase1;

import Base.WebDrive;
import org.openqa.selenium.By;

public class CreateAccountPage {

    WebDrive drive;

    private static final By Title = By.xpath("//div[@class=\"radio-inline\"][1]//input[@name=\"id_gender\"]");
    private static final By FirstName = By.xpath("//input[@name=\"customer_firstname\"]");
    private static final By LastName = By.xpath("//input[@name=\"customer_lastname\"]");
    private static final By Password = By.xpath("//input[@name=\"passwd\"]");
    private static final By FirstNameAddress = By.xpath("//input[@name=\"firstname\"]");
    private static final By LastNameAddress = By.xpath("//input[@name=\"lastname\"]");
    private static final By Address = By.xpath("//input[@name=\"address1\"]");
    private static final By City = By.xpath("//input[@name=\"city\"]");
    private static final By State = By.xpath("//select[@name=\"id_state\"]");
    private static final By Zip = By.xpath("//input[@name=\"postcode\"]");
    private static final By MobilePhone = By.xpath("//input[@name=\"phone_mobile\"]");
    private static final By RegisterButton = By.xpath("//button[@name=\"submitAccount\"]");


    public CreateAccountPage (WebDrive drive){
        this.drive = drive;
    }

    public void CreateAccount(){
        drive.click(Title);
        drive.sendkey(FirstName,"Mai");
        drive.sendkey(LastName, "Mai");
        drive.sendkey(Password,"12345");
        drive.sendkey(FirstNameAddress,"Mai");
        drive.sendkey(LastNameAddress,"Mai");
        drive.sendkey(Address,"4298  Tecumsah Lane");
        drive.sendkey(City,"LAKEWOOD");
        drive.DropDown(State,2);
        drive.sendkey(Zip,"90714");
        drive.sendkey(MobilePhone,"562-502-6546");
        drive.click(RegisterButton);
    }

}
