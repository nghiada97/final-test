package TestCase1;

import Base.WebDrive;
import org.openqa.selenium.By;

public class NewTab {

    WebDrive drive;

    private static final By EmailFiel = By.xpath("//input[@type=\"email\"]");
    private static final By PasswordFiel = By.xpath("//input[@type=\"password\"]");
    private static final By Email = By.xpath("//div[@role=\"tabpanel\"][1]//div[@class='Cp'][2]//table[@role=\"grid\"]//tr[1]");

    public NewTab(WebDrive drive){
        this.drive = drive;
    }

    public void CheckEmail(){
        drive.OpennewTab();
        drive.SwitchTab(1);
        drive.getDriverChrome().get("https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
        drive.sendkey(EmailFiel,"moneylove9971");
        drive.Enter(EmailFiel);
        drive.sendkey(PasswordFiel,"1593578426");
        drive.Enter(PasswordFiel);

    }

}
